/*jshint node: true */
"use strict";

var Promise = require('bluebird');
var fs = require("fs");

var userLogsFilePath = __dirname + "/../userLogs.json";

var userLogs = function() {
    return new Promise(function(resolve, reject) {
        fs.readFile(userLogsFilePath, 'utf8', function(err, data) {
            if (err) return resolve(new UserLogsFile());
            return resolve(new UserLogsFile(JSON.parse(data)));
        });
    });
};
exports.userLogs = userLogs;

var UserLogsFile = function(obj) {
    this.logs = [];
    this.save = function() {
        var fileToSave = this;
        return new Promise(function(resolve, reject) {
            fs.writeFile(userLogsFilePath, JSON.stringify(fileToSave, null, 4), "utf8", function(err) {
                if (err) return reject(err);
                return resolve(fileToSave);
            });
        });
    };
    this.clean = function() { // Cleans older than 1 month
        var minDate = new Date((new Date()).getTime() - (1000 * 60 * 60 * 24 * 30));
        for (var i = this.logs.length - 1; i >= 0; i--) {
            if (new Date(this.logs[i].date) < minDate) {
                this.logs.splice(i, 1);
            }
        }
    };
    for (var prop in obj) this[prop] = obj[prop];
};
exports.UserLogsFile = UserLogsFile;

var UserLog = function(username, server, domain, action, value) {
    this.username = username;
    this.server = server;
    this.domain = domain;
    this.action = action;
    this.value = value;
    this.date = new Date();
    this.pretty = function() {
        var header = this.date + " | " + this.username + ": ";
        var data = [this.server, this.domain, this.action, this.value];
        data = data.filter(function(n) {
            return n !== undefined;
        });
        return header + data.join(" -> ");
    };
};
exports.UserLog = UserLog;

var log = function(req, server, domain, action, value) {
    var username = "guest";
    if (req.user && req.user.username) username = req.user.username;
    if (typeof req === 'string' || req instanceof String) username = req;
    var userLog = new UserLog(
        username, server, domain, action, value
    );
    userLogs().then(function(userLogs) {
        userLogs.logs.push(userLog);
        return userLogs.save();
    }).then(function(userLogs) {
        console.log(userLog.pretty());
    }).catch(function(err) {
        console.error("Error logging: " + err);
    });
};
exports.log = log;

var logConsole = function(req, server, domain, action, value) {
    var username = "guest";
    if (req.user && req.user.username) username = req.user.username;
    var userLog = new UserLog(
        username, server, domain, action, value
    );
    console.log(userLog.pretty());
};
exports.logConsole = logConsole;
