/*jshint node: true */
"use strict";

var Promise = require('bluebird');
var fs = require("fs");

var configFilePath = __dirname + "/../config.json";

var config = function() {
    return new Promise(function(resolve, reject) {
        fs.readFile(configFilePath, 'utf8', function(err, data) {
            if (err) return resolve(new ConfigFile());
            return resolve(new ConfigFile(JSON.parse(data)));
        });
    });
};
exports.config = config;

var ConfigFile = function(obj) {
    this.name = "";
    this.mainPort = 8000;
    this.serverRoot = "../";
    this.users = [];
    this.servers = [];
    this.configVersion = 1;
    this.save = function() {
        var fileToSave = this;
        return new Promise(function(resolve, reject) {
            fs.writeFile(configFilePath, JSON.stringify(fileToSave, null, 4), "utf8", function(err) {
                if (err) return reject(err);
                return resolve(fileToSave);
            });
        });
    };
    this.findUser = function(username) {
        for (var i in this.users) {
            if (this.users[i].username == username) {
                return this.users[i];
            }
        }
        return null;
    };
    this.findServer = function(name) {
        for (var i in this.servers) {
            if (this.servers[i].name == name) {
                return this.servers[i];
            }
        }
        return null;
    };
    this.addServer = function(server) {
        this.servers.push(server);
    };
    this.removeServer = function(name) {
        for (var i in this.servers) {
            if (this.servers[i].name == name) {
                this.servers.splice(i, 1);
                return true;
            }
        }
        return false;
    };
    this.addUser = function(user) {
        this.users.push(user);
    };
    this.removeUser = function(username) {
        for (var i in this.users) {
            if (this.users[i].username == username) {
                this.users.splice(i, 1);
                return true;
            }
        }
        return false;
    };
    for (var prop in obj) this[prop] = obj[prop];
};
exports.ConfigFile = ConfigFile;

var User = function(name, username, password, role) {
    this.name = name;
    this.username = username;
    this.password = password;
    this.role = role;
};
exports.User = User;

var Server = function() {
    this.dateAdded = formatDate(new Date());
    this.token = uid(128);
    this.enabled = true;
    this.service = {};
    this.route = {};
};
exports.Server = Server;

var ServerServiceNode = function(script, instances, maxMemory) {
    this.type = "node";
    this.script = script;
    this.instances = instances;
    this.maxMemory = maxMemory;
};
exports.ServerServiceNode = ServerServiceNode;

var ServerRoute = function(type, path, destination) {
    this.type = type;
    this.path = path;
    this.destination = destination;
};
exports.ServerRoute = ServerRoute;


var uid = function(len) {
    var buf = [],
        chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
        charlen = chars.length;
    for (var i = 0; i < len; ++i) {
        buf.push(chars[getRandomInt(0, charlen - 1)]);
    }
    return buf.join('');
};

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


var formatDate = function(date) {
    var day = date.getDate();
    var month = date.getMonth()+1;
    var year = date.getFullYear();
    return pad(day,2) + '/' + pad(month,2) + '/' + year;
};
var pad = function(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
};
