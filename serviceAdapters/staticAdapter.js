/*jshint node: true */
"use strict";

var Promise = require('bluebird');
var configModel = require('../storageModels/configModel.js');
var userLogs = require('../storageModels/userLogsModel.js');
var request = require('request');


var status = function(req, server) {
    return new Promise(function(resolve, reject) {
        if (!server.route.type) return resolve('black');
        request('http://localhost:8000'+server.route.path, function(error, response, body) {
            if (error || response.statusCode != 200) return resolve('red');
            return resolve('green');
        });
    });
};
exports.status = status;

var start = function(req, server) {
    return new Promise(function(resolve, reject) {
        return reject("Action not available");
    });
};
exports.start = start;

var stop = function(req, server) {
    return new Promise(function(resolve, reject) {
        return reject("Action not available");
    });
};
exports.stop = stop;

var restart = function(req, server) {
    return new Promise(function(resolve, reject) {
        return reject("Action not available");
    });
};
exports.restart = restart;

var update = function(req, server) {
    return new Promise(function(resolve, reject) {
        return reject("Action not available");
    });
};
exports.update = update;

var deploy = function(req, server) {
    return new Promise(function(resolve, reject) {
        return reject("Action not available");
    });
};
exports.deploy = deploy;
