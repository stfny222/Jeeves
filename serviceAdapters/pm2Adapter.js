/*jshint node: true */
"use strict";

var Promise = require('bluebird');
var pm2 = require('pm2'); // http://pm2.keymetrics.io/docs/usage/pm2-api/
var exec = require('child_process').exec;
var configModel = require('../storageModels/configModel.js');
var userLogs = require('../storageModels/userLogsModel.js');

var status = function(req, server) {
    return new Promise(function(resolve, reject) {
        if (!server.service) return resolve('black');
        if (!server.service.type || server.service.type != "node") return resolve('blue');
        pm2.connect(function(err) {
            if (err) return resolve('red');
            pm2.describe(server.name, function(err, data) {
                if (err) return resolve('red');
                if (!data || data.length === 0) return resolve('red');
                if (data[0].pm2_env.status == 'online') return resolve('green');
                if (data[0].pm2_env.status == 'errored') return resolve('red');
                if (data[0].pm2_env.status == 'stopped') return resolve('orange');
                return resolve('red');
            });
        });
    });
};
exports.status = status;

var start = function(req, server) {
    return new Promise(function(resolve, reject) {
        if (!server.service) return reject("No service found");
        if (!server.service.type || server.service.type != "node") return reject("Service is not node");
        configModel.config().then(function(config) {
            pm2.connect(function(err) {
                if (err) return reject(err);
                pm2.start({
                    name: server.name,
                    script: config.serverRoot + server.name + server.service.script,
                    exec_mode: (server.service.instances == 1 ? "fork" : "cluster"),
                    instances: server.service.instances,
                    max_memory_restart: server.service.maxMemory
                }, function(err, data) {
                    if (err) return reject(err);
                    userLogs.log(req, server.name, "PM2", "Start", "Success");
                    return resolve("Started!");
                });
            });
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.start = start;
var stop = function(req, server) {
    return new Promise(function(resolve, reject) {
        if (!server.service) return reject("No service found");
        if (!server.service.type || server.service.type != "node") return reject("Service is not node");
        pm2.connect(function(err) {
            if (err) return reject(err);
            pm2.stop(server.name, function(err, data) {
                if (err) return reject(err);
                userLogs.log(req, server.name, "PM2", "Stop", "Success");
                return resolve("Stopped!");
            });
        });
    });
};
exports.stop = stop;

var restart = function(req, server) {
    return new Promise(function(resolve, reject) {
        if (!server.service) return reject("No service found");
        if (!server.service.type || server.service.type != "node") return reject("Service is not node");
        pm2.connect(function(err) {
            if (err) return reject(err);
            pm2.restart(server.name, function(err, data) {
                if (err) return reject(err);
                userLogs.log(req, server.name, "PM2", "Restart", "Success");
                return resolve("Restarted!");
            });
        });
    });
};
exports.restart = restart;

var update = function(req, server) {
    return restart(req, server);
};
exports.update = update;

var deploy = function(req, server) {
    return new Promise(function(resolve, reject) {
        if (!server.service) return reject("No service found");
        if (!server.service.type || server.service.type != "node") return reject("Service is not node");
        configModel.config().then(function(config) {
            exec('cd ' + __dirname + '/../' + config.serverRoot + server.name + ' && npm install && echo \'success\'',
                function(err, stdout, stderr) {
                    if (err) return reject(err);
                    if (stdout != "success\n") return reject("Output: " + stdout);
                    userLogs.log(req, server.name, "PM2", "Deploy", "Success");
                    return resolve("Deployed!");
                });
        }).catch(function(err) {
            return reject(err);
        });
    });
};
exports.deploy = deploy;
