/*jshint node: true */
"use strict";

var Promise = require('bluebird');
var configModel = require('./storageModels/configModel.js');
var sessionsModel = require('./storageModels/sessionsModel.js');
var userLogs = require('./storageModels/userLogsModel.js');
var crypto = require('crypto');

var login = function(req, username, password) {
    return new Promise(function(resolve, reject) {
        configModel.config().then(function(config) {
            var passwordHash = hashWithSalt(username, password);
            var user = config.findUser(username);
            if (!user || user.password !== passwordHash) {
                throw "Username or password incorrect";
            }
            return [sessionsModel.sessions(), user];
        }).spread(function(sessions, user) {
            var expiration = new Date((new Date()).getTime() + (1000 * 60 * 60 * 2));
            var token = new sessionsModel.SessionToken(
                user.username, uid(256), expiration, req.headers["user-agent"], req.sessionID
            );
            sessions.clean();
            sessions.addToken(token);
            return [sessions.save(), user, token];
        }).spread(function(sessions, user, token) {
            userLogs.log(req, "Auth", "Login", "Succeeded", "(" + username + ")");
            return resolve({
                "user": {
                    "username": user.username,
                    "name": user.name,
                    "role": user.role
                },
                "token": {
                    "value": token.value,
                    "expiration": token.expiration
                }
            });
        }).catch(function(err) {
            userLogs.log(req, "Auth", "Login", "Failed", "(" + username + ")");
            return reject(err);
        });
    });
};
exports.login = login;

var logout = function(req) {
    return new Promise(function(resolve, reject) {
        if (!req.headers.token) return reject("No token");
        var username = null;
        sessionsModel.sessions().then(function(sessions) {
            username = sessions.removeToken(req.headers.token);
            sessions.clean();
            return [sessions.save(), username];
        }).spread(function(sessions, username) {
            userLogs.log(req, "Auth", "Logout", "Succeeded", "(" + username + ")");
            return resolve({
                "result": "success"
            });
        }).catch(function(err) {
            userLogs.log(req, "Auth", "Logout", "Failed", "(" + username + ")");
            return reject(err);
        });
    });
};
exports.logout = logout;

//gabo valentina isabella litta enrique fiorella flor gabriel manu francesca huby muri manuel dorita maria carmen nirvana litia.
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-http-bearer').Strategy;

passport.use('basic', new BasicStrategy({
    passReqToCallback: true
}, function(req, username, password, done) {
    configModel.config().then(function(config) {
        var user = config.findUser(username);
        if (!user || user.password !== password) return done(null, false);
        return done(null, user);
    }).catch(function(err) {
        return done(err, null);
    });
}));

var authenticateToken = function(req, tokenValue, requiredRole) {
    return new Promise(function(resolve, reject) {
        sessionsModel.sessions().then(function(sessions) {
            var token = sessions.findToken(tokenValue);
            if (!token) return reject("Session not valid");
            if (new Date(token.expiration) < new Date()) return reject("Session not valid");
            if (token.userAgent != req.headers["user-agent"]) return reject("Session not valid");
            //if (token.browserSession != req.sessionID) return reject("Session not valid");
            return [configModel.config(), token];
        }).spread(function(config, token) {
            var user = config.findUser(token.username);
            if (!user) return reject("Session not valid");
            if (requiredRole) {
                var requiredRoleNum = roleToNumber(requiredRole);
                var userRoleNum = roleToNumber(user.role);
                if (userRoleNum < requiredRoleNum) return reject("Insufficent priviledges");
            }
            return resolve(user);
        }).catch(function(err) {
            return reject(err);
        });
    });
};

var roleToNumber = function(role) {
    switch (role) {
        case 'master':
            return 9;
        case 'admin':
            return 7;
        case 'user':
            return 5;
        case 'viewer':
            return 2;
    }
};
exports.roleToNumber = roleToNumber;

var hashWithSalt = function(user, pass) {
    var hash = crypto.createHash('sha256');
    return hash.update(user + "|" + pass + "ADDsomeSALT").digest('hex');
};
exports.hashWithSalt = hashWithSalt;

passport.use('bearerAny', new BearerStrategy({
    passReqToCallback: true
}, function(req, tokenValue, done) {
    authenticateToken(req, tokenValue, null).then(function(user) {
        return done(null, user);
    }).catch(function(err) {
        return done(err, null);
    });
}));

passport.use('bearerMaster', new BearerStrategy({
    passReqToCallback: true
}, function(req, tokenValue, done) {
    authenticateToken(req, tokenValue, 'master').then(function(user) {
        return done(null, user);
    }).catch(function(err) {
        return done(err, null);
    });
}));

passport.use('bearerAdmin', new BearerStrategy({
    passReqToCallback: true
}, function(req, tokenValue, done) {
    authenticateToken(req, tokenValue, 'admin').then(function(user) {
        return done(null, user);
    }).catch(function(err) {
        return done(err, null);
    });
}));

passport.use('bearerUser', new BearerStrategy({
    passReqToCallback: true
}, function(req, tokenValue, done) {
    authenticateToken(req, tokenValue, 'user').then(function(user) {
        return done(null, user);
    }).catch(function(err) {
        return done(err, null);
    });
}));

var isAuthenticatedAny = passport.authenticate(['bearerAny'], {
    session: false
});
exports.isAuthenticatedAny = isAuthenticatedAny;

var isAuthenticatedMaster = passport.authenticate(['bearerMaster'], {
    session: false
});
exports.isAuthenticatedMaster = isAuthenticatedMaster;

var isAuthenticatedAdmin = passport.authenticate(['bearerAdmin'], {
    session: false
});
exports.isAuthenticatedAdmin = isAuthenticatedAdmin;

var isAuthenticatedUser = passport.authenticate(['bearerUser'], {
    session: false
});
exports.isAuthenticatedUser = isAuthenticatedUser;

passport.use('direct', new BearerStrategy({
    passReqToCallback: true
}, function(req, tokenValue, done) {
    if (!req.params.name) return done(null, false);
    if (!tokenValue) return done(null, false);
    configModel.config().then(function(config) {
        var server = config.findServer(req.params.name);
        if (!server) return done(null, false);
        if (!server.token) return done(null, false);
        if (server.token != tokenValue) return done(null, false);
        var user = new configModel.User("Hook", "hook", "hook", "master");
        return done(null, user);
    }).catch(function(err) {
        return done(err, null);
    });
}));

var isAuthenticatedDirect = passport.authenticate(['direct'], {
    session: false
});
exports.isAuthenticatedDirect = isAuthenticatedDirect;

var uid = function(len) {
    var buf = [],
        chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
        charlen = chars.length;
    for (var i = 0; i < len; ++i) {
        buf.push(chars[getRandomInt(0, charlen - 1)]);
    }
    return buf.join('');
};

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
